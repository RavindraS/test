FROM alpine:latest
RUN apk --no-cache add apache2
MAINTAINER Ravindra
CMD ["/usr/sbin/httpd", "-D", "FOREGROUND"]

